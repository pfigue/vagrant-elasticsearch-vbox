include wget


service { "elasticsearch":
#  provider => 'init',
  enable  => true,
  ensure  => running,
  start   => '/etc/init.d/elasticsearch start',
  stop    => '/etc/init.d/elasticsearch stop',
  restart => '/etc/init.d/elasticsearch restart',
  status  => '/etc/init.d/elasticsearch status',
  subscribe => [
    Exec['install ES'],
    File['/etc/elasticsearch/elasticsearch.yml'],
    File['/etc/init.d/elasticsearch'],
    ]
}

file { "/etc/elasticsearch/elasticsearch.yml":
  ensure  => file,
  content => template("elasticsearch/elasticsearch.yml"),
  require => Exec['install ES'],
}

# Similar to the package init.d script,
# but provides ES_JAVA_OPTIONS=-Djava.net.preferIPv4Stack=true
# to ask the JVM to use only IPv4 instead of IPv6 (default)
file { "/etc/init.d/elasticsearch":
  ensure  => file,
  mode    => 'a+rx,u+w',
  content => template("elasticsearch/elasticsearch"),
  require => Exec['install ES'],
}


exec { "install ES":
  command => "/usr/bin/dpkg -i /vagrant/elasticsearch-1.1.0.deb",
  require => [Package['openjdk-7-jre-headless'], Wget::Fetch['Download ElasticSearch 1.1.0 Debian Package'],]
}

wget::fetch { "Download ElasticSearch 1.1.0 Debian Package":
  source      => 'https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.1.0.deb',
  destination => '/vagrant/elasticsearch-1.1.0.deb',
  timeout     => 0,
  verbose     => true,
}

package { ['openjdk-7-jre-headless', 'vim', 'curl', 'tmux', 'jq', 'git']:
	ensure  => installed,
	require => Exec['apt-get update']
}

exec { 'apt-get update':
  command => "/usr/bin/apt-get update"
}

