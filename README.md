# What is vagrant-elasticsearch-vbox ?
It is a [Vagrant](http://www.vagrantup.com/) setup to provide me with an easy way to start using ElasticSearch.

# How easy is to use vagrant-elasticsearch-vbox ?
Supereasy. For example...

### Clone the repo and fetch the modules

    $ git clone git@bitbucket.org:pfigue/vagrant-elasticsearch-vbox.git
    $ cd vagrant-elasticsearch-vbox/
    $ git submodule init
    $ git submodule update

### Setup the machine

    $ vagrant up
    Bringing machine 'default' up with 'virtualbox' provider...
    [default] Importing base box 'precise64'...
    [default] Matching MAC address for NAT networking...
    [default] Setting the name of the VM...
    [default] Clearing any previously set forwarded ports...
    [default] Clearing any previously set network interfaces...
    [default] Preparing network interfaces based on configuration...
    [default] Forwarding ports...
    [default] -- 22 => 2222 (adapter 1)
    [default] -- 9200 => 9200 (adapter 1)
    [default] -- 9300 => 9300 (adapter 1)
    [default] -- 9500 => 9500 (adapter 1)
    [default] Booting VM...
    [default] Waiting for machine to boot. This may take a few minutes...
    [default] Machine booted and ready!
    [default] Mounting shared folders...
    [default] -- /vagrant
    [default] -- /tmp/vagrant-puppet-1/manifests
    [default] -- /tmp/vagrant-puppet-1/modules-0
    [default] Running provisioner: puppet...
    Running Puppet with site.pp...
    stdin: is not a tty
    notice: /Stage[main]//Exec[apt-get update]/returns: executed successfully
    notice: /Stage[main]//Package[openjdk-7-jre-headless]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Package[vim]/ensure: ensure changed 'purged' to 'present'
    notice: /Stage[main]//Exec[install ES]/returns: executed successfully
    notice: /Stage[main]//File[/etc/init.d/elasticsearch]/content: content changed '{md5}130a053aa4c18212bceede4ca081314f' to '{md5}31579a67046c3b01d57e3db4a91c9ff9'
    notice: /Stage[main]//File[/etc/elasticsearch/elasticsearch.yml]/content: content changed '{md5}e0ea794da86d89b7e1b8192504349313' to '{md5}478443792e8897893b817f495e142924'
    notice: /Stage[main]//Service[elasticsearch]/ensure: ensure changed 'stopped' to 'running'
    notice: /Stage[main]//Service[elasticsearch]: Triggered 'refresh' from 3 events
    notice: Finished catalog run in 63.05 seconds


### Test it

Requesting a non existent document:

    $ curl -s 'http://localhost:9200/twitter/tweet/1' | jq '.'
    {
      "status": 404,
      "error": "IndexMissingException[[twitter] missing]"
    }


Creating that document:

    $ curl -s -XPUT 'http://localhost:9200/twitter/tweet/1' -d '{
        "user" : "kimchy",
        "post_date" : "2009-11-15T14:12:12",
        "message" : "trying out Elasticsearch"
    }' | jq '.'
    {
      "created": true,
      "_version": 1,
      "_id": "1",
      "_type": "tweet",
      "_index": "twitter"
    }

Asking for it again:

    $ curl -s 'http://localhost:9200/twitter/tweet/1' | jq '.'
    {
      "_source": {
        "message": "trying out Elasticsearch",
        "post_date": "2009-11-15T14:12:12",
        "user": "kimchy"
      },
      "found": true,
      "_version": 3,
      "_id": "1",
      "_type": "tweet",
      "_index": "twitter"
    }

Stop and destroy the machine:

    $ vagrant destroy
    Are you sure you want to destroy the 'default' VM? [y/N] y
    [default] Forcing shutdown of VM...
    [default] Destroying VM and associated drives...
    [default] Running cleanup tasks for 'puppet' provisioner...

If you ask now for that document, you get an error:

    $ curl -s 'http://localhost:9200/twitter/tweet/1'
    $ echo $?
    7


# Notes and more details

+ The previous installation steps rely on several tools, like [Vagrant](http://www.vagrantup.com/) and [jq](http://stedolan.github.io/jq/).
+ It uses Virtual Box as virtualization tool.
    + LXC would be much nicer for Linux users.
+ The base box is *precise64*, so **Ubuntu 12.04 LTS amd64**.
+ Ports 9200/tcp, 9300/tcp and 9500/tcp are forwarded from host to guest.
    + Nonetheless, Thrift protocol (9500/tcp) is not enabled at the moment.
    + Java VM is listening only on IPv4 sockets, not in IPv6.
+ There is only one ES instance.
    + The setup is intended to play a little bit with ES, as a developer.
    + Writing a second setup with 2 or 3 VMs and letting the respective ES instances share data between them could be also cool.
+ The puppet manifests depend on [maestrodev/wget puppet module](https://forge.puppetlabs.com/maestrodev/wget) version 1.4.1;
    + There is also an [elasticsearch/elasticsearch puppet module](https://forge.puppetlabs.com/elasticsearch/elasticsearch), which can install ES as well.
